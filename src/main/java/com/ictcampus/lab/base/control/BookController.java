package com.ictcampus.lab.base.control;

import com.ictcampus.lab.base.control.model.BookRequest;
import com.ictcampus.lab.base.control.model.BookResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
//prove
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/books")
@AllArgsConstructor
@Slf4j

/*
0

  @author Pipposultavolino
 * @since 1.0.13.0
 */
public class BookController {
	private List<BookResponse> list = generateBooks();

	public BookController() {
		this.list = generateBooks();
	}

	@GetMapping(value = "/init", produces = MediaType.APPLICATION_JSON_VALUE)
	public String init() {
		list = generateBooks();
		return "Init OK";
	}

	@GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<BookResponse> getBooks() {
		log.info("Restituisco la lista dei libri [{}]", list);
		return list;
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public BookResponse getBook(@PathVariable(name = "id") Long id) {
		BookResponse bookResponse = findBookById(id);
		if (bookResponse == null) {
			log.info("Libro con ID [{}] non trovato", id);
			throw new BookNotFoundException();
		}
		return bookResponse;
	}

	@PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public Long createBook(@RequestBody BookRequest bookRequest) {
		log.info("Creo un nuovo libro con i dati [{}]", bookRequest);

		Long newId = generateNewId();
		log.info("ID del nuovo libro con i dati [{}]", newId);

		list.add(BookResponse.builder()
				.id(newId)
				.title(bookRequest.getTitle())
				.author(bookRequest.getAuthor())
				.ISDB(bookRequest.getISDB())
				.build());

		return newId;
	}

	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void updateBook(@PathVariable(name = "id") Long id, @RequestBody BookRequest bookRequest) {
		log.info("Aggiorno il libro con ID [{}] e dati [{}]", id, bookRequest);

		BookResponse bookResponse = findBookById(id);
		if (bookResponse != null) {
			bookResponse.setTitle(bookRequest.getTitle());
			bookResponse.setAuthor(bookRequest.getAuthor());
			bookResponse.setISDB(bookRequest.getISDB());
			log.info("Libro con ID [{}] aggiornato con successo", id);
		} else {
			log.warn("Libro con ID [{}] non trovato", id);
			throw new BookNotFoundException();
		}
	}

	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void deleteBook(@PathVariable(name = "id") Long id) {
		log.info("Cancello il libro con ID [{}]", id);

		boolean removed = list.removeIf(book -> book.getId().equals(id));
		if (removed) {
			log.info("Libro con ID [{}] eliminato correttamente", id);
		} else {
			log.warn("Libro con ID [{}] non trovato o già eliminato", id);
			throw new BookNotFoundException();
		}
	}

	private BookResponse findBookById(Long id) {
		Optional<BookResponse> bookResponseOptional = list.stream()
				.filter(book -> book.getId().equals(id))
				.findFirst();
		return bookResponseOptional.orElse(null);
	}

	private Long generateNewId() {
		return list.stream()
				.map(BookResponse::getId)
				.max(Long::compareTo)
				.orElse(0L) + 1;
	}

	private List<BookResponse> generateBooks() {
		List<BookResponse> books = new ArrayList<>();
		books.add(BookResponse.builder().id(1L).title("Title 1").author("Author 1").ISDB("ISDB 1").build());
		books.add(BookResponse.builder().id(2L).title("Title 2").author("Author 2").ISDB("ISDB 2").build());
		books.add(BookResponse.builder().id(3L).title("Title 3").author("Author 3").ISDB("ISDB 3").build());
		return books;
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	private static class BookNotFoundException extends RuntimeException {
		public BookNotFoundException() {
			super("Book not found");
		}
	}
}
