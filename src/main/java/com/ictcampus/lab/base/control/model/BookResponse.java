package com.ictcampus.lab.base.control.model;

import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@Jacksonized
public class BookResponse {
	private Long id;
	private String title;
	private String author;
	private String ISDB;
}
