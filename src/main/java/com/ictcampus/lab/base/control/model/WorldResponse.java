package com.ictcampus.lab.base.control.model;


import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

/**
 * TODO Add Class Description
 *
 * @author Pipposultavolino
 * @since 1.0.0
 */
@Getter
@Builder
@Jacksonized
@Data
public class WorldResponse {
	private Long id;
	private String name;
	private String system;
}
